/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exemploadapter;

import com.mycompany.exemploadapter.Interface.CalculoQuimico;

/**
 *
 * @author Andrey e Breno
 */
public class DensidadeAdapter implements CalculoQuimico{
    private Densidade adapte;
        
    public DensidadeAdapter(Densidade densidade){
        this.adapte = densidade;
    }
        
    @Override
    public void calcular(double x, double y){
        adapte.calcular(x, y);
    }
}
