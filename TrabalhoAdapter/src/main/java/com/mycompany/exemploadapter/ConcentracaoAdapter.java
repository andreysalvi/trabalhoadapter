/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exemploadapter;

import com.mycompany.exemploadapter.Interface.CalculoQuimico;

/**
 *
 * @author Andrey e Breno
 */
public class ConcentracaoAdapter implements CalculoQuimico{
        private Concentracao adapte;
        
        public ConcentracaoAdapter(Concentracao concentracao){
            this.adapte = concentracao;
        }
        
        @Override
        public void calcular(double x, double y){
            x *= 0.0001;
            y *= 0.0001;
            adapte.calcular(x, y);
        }
    }
