/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exemploadapter;

/**
 *
 * @author Andrey e Breno
 */
public class Concentracao {
    public void calcular(double x, double y){
        System.out.println("A concentração da soluçao é de " + x + 
            " miligramas de soluto para " + y + " mililitros de solução."
                    + "C = " + (x/y) + " mg/ml");
    }
}
