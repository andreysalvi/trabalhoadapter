/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exemploadapter;

import com.mycompany.exemploadapter.Interface.CalculoQuimico;

/**
 *
 * @author Andrey e Breno
 */
public class Main {
    public static void main(String[] args) {
        CalculoQuimico[] cq = {
            new DensidadeAdapter(new Densidade()),
            new ConcentracaoAdapter(new Concentracao())
        };
        
        double x = 15.0, y = 28.9;
        for (CalculoQuimico calculoQuimico : cq) {
            calculoQuimico.calcular(x, y);
        }
    }
}
